#!/usr/bin/python

import os

import buildbot
import buildbot.process.factory
from buildbot.steps.source.git import Git

from AnnotatedCommand import AnnotatedCommand

def getGlibcAnnotatedFactory(
    clean=False,
    env=None,
    timeout=1200):

    merged_env = {}

    # Use env variables defined in the system.
    merged_env.update(os.environ)
    # Clobber bot if we need a clean build.
    if clean:
        merged_env['BUILDBOT_CLOBBER'] = '1'
    # Overwrite pre-set items with the given ones, so user can set anything.
    if env is not None:
        merged_env.update(env)

    f = buildbot.process.factory.BuildFactory()

    # Determine the build directory.
    f.addStep(buildbot.steps.shell.SetProperty(name='get_builddir',
                                               command=['pwd'],
                                               property='builddir',
                                               description='set build dir',
                                               workdir='.',
                                               env=merged_env))


    # Get buildbot scripts.
    f.addStep(Git(name='update scripts',
                  alwaysUseLatest=True,
                  repourl='git://sourceware.org/git/glibc-buildbot.git',
                  retry=(120, 10),
                  retryFetch=True,
                  workdir='glibc-buildbot'))

    selector_script = os.path.join('..', 'glibc-buildbot',
                                   'scripts', 'slave', 'buildbot_selector.py')

    # Run annotated command.
    f.addStep(AnnotatedCommand(name='annotate',
                               description='annotate',
                               timeout=timeout,
                               haltOnFailure=True,
                               command='python ' + selector_script,
                               env=merged_env))
    return f
