#!/bin/bash

set -x
set -u

configure_args=(
  --prefix=/usr
  --enable-add-ons
  "$@"
)

nproc=$(getconf _NPROCESSORS_ONLN)

root_dir=$(pwd)
src_dir="${root_dir}/glibc"
build_dir="${root_dir}/build"

git_branch=${BUILDBOT_BRANCHNAME:-master}
git_revision=${BUILDBOT_REVISION:-origin/${git_branch}}

num_jobs_build=${nproc:-1}
# Some make bug makes it often wedge in parallel mode.
num_jobs_check=1

# BUILDBOT_CLOBBER=1 is in the environment if the Buildbot
# infrastructure is explicitly requesting a clobber build.
clobber=false
if [ -n "${BUILDBOT_CLOBBER:-}" ]; then
  clobber=true
fi

start_step() {
  local say_clobber=
  if $clobber; then
    say_clobber=' (clobber)'
  fi
  echo "@@@BUILD_STEP $*${say_clobber}@@@"
}

end_step() {
  local rc=$?
  if [ $rc -ne 0 ]; then
    echo '@@@STEP_FAILURE@@@'
  fi
  return $rc
}

do_sync() {
  start_step sync
  (set -e
   [ -d "${src_dir}" ] ||
     git clone git://sourceware.org/git/glibc.git ${src_dir}
   cd "${src_dir}"
   git remote prune origin
   git remote update --prune
   git clean -dffx
   git checkout --detach -f "${git_revision}"
  )
  end_step
}

do_configure() {
  start_step configure
  mkdir -p "$build_dir"
  (cd "$build_dir" && "${src_dir}"/configure "${configure_args[@]}")
  end_step
}

do_build() {
  start_step make
  make -C "${build_dir}" -j${num_jobs_build} -k
  end_step
}

do_check() {
  start_step check
  make -C "${build_dir}" -j${num_jobs_check} -k check
  end_step
}

do_clobber() {
  clobber=true
  cd "$root_dir"
  rm -rf "$build_dir"
}


do_whole_build() {
  do_configure &&
  do_build &&
  do_check
}

###

do_sync || exit

if [ -d "${build_dir}" ] && ! $clobber; then
  need_clobber=true
else
  need_clobber=false
fi

do_whole_build
rc=$?

if [ $rc -ne 0 ] && $need_clobber; then
  do_clobber && do_whole_build
  rc=$?
fi

exit $rc
